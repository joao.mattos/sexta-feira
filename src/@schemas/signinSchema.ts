import { z } from "zod";

export const signinSchema = z.object({
  username: z
    .string()
    .min(5, {
      message: "A matrícula precisa ter pelo menos 5 caracteres.",
    })
    .max(7, "Tamanho máximo de caractéres excedido"),
  password: z.string().min(8, {
    message: "A senha precisa ter pelo menos 8 caracteres.",
  }),
});
