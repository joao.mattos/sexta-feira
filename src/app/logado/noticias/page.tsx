"use client";
import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import Link from "next/link";
import axios from "axios";
import { useEffect, useState } from "react";
import { NoticiaProps, NoticiaResponseProps } from "@/@types/noticia";

export default function Noticias() {
  const [artigos, setArtigos] = useState<Array<NoticiaProps>>([]);
  const [paisAtual, setPaisAtual] = useState("br");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function carregarArtigos() {
      setLoading(true);
      const response = await axios.get(
        `https://newsapi.org/v2/top-headlines?country=${paisAtual}&apiKey=`
        // adicionar chave de acesso da newsapi
      );
      const nResponse: NoticiaResponseProps = response.data;
      setArtigos(nResponse.articles);

      setLoading(false);
    }
    carregarArtigos();
  }, [paisAtual]);

  if (loading) {
    return <p>carregando...</p>;
  }

  return (
    <div className="p-4 w-full h-full flex flex-col">
      <h2 className="text-3xl font-bold mb-1">Notícias</h2>
      <p className="mb-6">{artigos.length} artigos encontrados.</p>
      <div className="flex items-center gap-3 mb-3">
        <Button
          variant={paisAtual == "br" ? "secondary" : "outline"}
          onClick={() => setPaisAtual("br")}
        >
          Brasil
        </Button>
        <Button
          variant={paisAtual == "us" ? "secondary" : "outline"}
          onClick={() => setPaisAtual("us")}
        >
          Estados Unidos
        </Button>
        <Button
          variant={paisAtual == "ca" ? "secondary" : "outline"}
          onClick={() => setPaisAtual("ca")}
        >
          Canadá
        </Button>
        <Button
          variant={paisAtual == "ar" ? "secondary" : "outline"}
          onClick={() => setPaisAtual("ar")}
        >
          Argentina
        </Button>
      </div>
      <div className="flex items-stretch gap-6 flex-wrap">
        {artigos.length > 0 &&
          artigos.map((artigo, index) => (
            <Card
              className="max-w-96 w-full flex flex-col justify-between"
              key={index}
            >
              <CardHeader>
                <CardTitle>{artigo.title}</CardTitle>
                <CardDescription>{artigo.author}</CardDescription>
              </CardHeader>
              <CardContent>
                <p className="text-sm">{artigo?.description}</p>
              </CardContent>
              <CardFooter>
                <Link href={artigo.url} target="_blank">
                  <Button>Ler matéria completa</Button>
                </Link>
              </CardFooter>
            </Card>
          ))}
      </div>
    </div>
  );
}
