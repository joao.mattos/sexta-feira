"use client";

import { Button } from "@/components/ui/button";
import { cn } from "@/lib/utils";
import { HomeIcon, LogOut, Newspaper, User2Icon } from "lucide-react";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function Sidebar() {
  const pathname = usePathname();
  return (
    <aside className="border-r p-6 w-64 flex flex-col justify-between sticky top-20 h-[calc(100vh-5rem)]">
      <div>
        <ul className="flex flex-col gap-2">
          <Link href="/logado">
            <li
              className={cn(
                "flex gap-2 items-center p-3 rounded",
                pathname === "/logado" && "bg-muted"
              )}
            >
              <HomeIcon size={18} />
              <p>Início</p>
            </li>
          </Link>
          <Link href="/logado/perfil">
            <li
              className={cn(
                "flex gap-2 items-center p-3 rounded",
                pathname === "/logado/perfil" && "bg-muted"
              )}
            >
              <User2Icon size={18} />
              <p>Perfil</p>
            </li>
          </Link>
          <Link href="/logado/noticias">
            <li
              className={cn(
                "flex gap-2 items-center p-3 rounded",
                pathname === "/logado/noticias" && "bg-muted"
              )}
            >
              <Newspaper size={18} />
              <p>Notícias</p>
            </li>
          </Link>
        </ul>
      </div>
      <Button variant={"outline"} asChild>
        <Link href={"/"}>
          <LogOut size={18} className="mr-2" /> Sair
        </Link>
      </Button>
    </aside>
  );
}
