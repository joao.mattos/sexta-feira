"use client";

import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { DropdownMenuLabel } from "@radix-ui/react-dropdown-menu";
import { Code2Icon, LogOut, MoonIcon, SunIcon, UserCog2 } from "lucide-react";
import { useTheme } from "next-themes";
import Link from "next/link";

export default function Navbar() {
  const { theme, setTheme } = useTheme();

  return (
    <nav className="border-b bg-background px-12 py-4 flex items-center justify-between h-20 sticky top-0">
      <div className="flex gap-1 items-center">
        <p className="font-semibold text-muted-foreground">Repasse</p>
        <Code2Icon className="text-sky-400" size={20} />
      </div>
      <DropdownMenu>
        <DropdownMenuTrigger className="border rounded p-2">
          <UserCog2 />
        </DropdownMenuTrigger>
        <DropdownMenuContent className="mr-2 p-4 flex flex-col gap-2">
          <DropdownMenuLabel className="text-xs font-bold mb-2">
            Temas
          </DropdownMenuLabel>
          <DropdownMenuItem
            className="flex items-center gap-2"
            onClick={() => setTheme("dark")}
          >
            <MoonIcon size={18} /> <span>Escuro</span>
          </DropdownMenuItem>
          <DropdownMenuItem
            className="flex items-center gap-2"
            onClick={() => setTheme("light")}
          >
            <SunIcon size={18} /> <span>Claro</span>
          </DropdownMenuItem>
          <DropdownMenuLabel className="text-xs font-bold mb-2">
            Perfil
          </DropdownMenuLabel>
          <DropdownMenuItem asChild className="flex items-center gap-2">
            <Link href={"/"}>
              <LogOut size={18} /> <span>Sair</span>
            </Link>
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </nav>
  );
}
