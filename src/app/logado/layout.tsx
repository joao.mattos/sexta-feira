import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";

export default function ayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <div className="flex flex-col">
      <Navbar />
      <div className="flex">
        <Sidebar />
        <div className="p-6 w-full">{children}</div>
      </div>
    </div>
  );
}
