import { UserCog2Icon } from "lucide-react";

export default function Perfil() {
  return (
    <div className="bg-muted p-4 w-full h-full flex flex-col items-center justify-center gap-4">
      <UserCog2Icon size={56} />
      <p>Página de edição de perfil</p>
    </div>
  );
}
