"use client";
import Lottie from "lottie-react";
import tetris from "@/assets/tetris.json";

export default function Home() {
  return (
    <div className="flex min-h-[70vh] flex-col p-4 items-center justify-center">
      <Lottie animationData={tetris} className="max-w-96" />
    </div>
  );
}
