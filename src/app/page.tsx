"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Card } from "@/components/ui/card";
import { signinSchema } from "@/@schemas/signinSchema";
import { useState } from "react";
import { toast } from "sonner";
import { useRouter } from "next/navigation";

export default function Signin() {
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const form = useForm<z.infer<typeof signinSchema>>({
    resolver: zodResolver(signinSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  });

  async function onSubmit(values: z.infer<typeof signinSchema>) {
    setLoading(true);
    await new Promise((resolve) => setTimeout(resolve, 1000));

    if (values.password === "admin123") {
      router.push("/logado");
    } else {
      toast.error("Usuário ou senha incorretos");
    }

    setLoading(false);
  }

  return (
    <div className="w-full h-[100vh] flex items-center justify-center">
      <Card className="p-6 max-w-md w-full">
        <p className="font-semibold text-lg mb-4">Faça login para continuar </p>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
            <FormField
              control={form.control}
              name="username"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Matrícula</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a matrícula..."
                      {...field}
                      type="number"
                    />
                  </FormControl>
                  <FormDescription>Apenas números</FormDescription>
                  <FormMessage />
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Senha</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Digite a senha..."
                      {...field}
                      type="password"
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <Button className="w-full" disabled={loading}>
              {loading ? "Toc toc..." : "Entrar"}
            </Button>
          </form>
        </Form>
      </Card>
    </div>
  );
}
