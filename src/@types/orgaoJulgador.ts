import { LocalizacaoProps } from "./localizacao";

export interface OrgaoJulgadorProps {
  id: number;
  nome: string;
  sigla: string;
  ativo: boolean;
  localizcoes: Array<LocalizacaoProps>;
}
