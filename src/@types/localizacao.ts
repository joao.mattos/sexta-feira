export interface LocalizacaoProps {
  id: number;
  ativo: boolean;
  nome: string;
}
